package model;

public class Health extends Jauge {

    private static final int FULL_HEALTH = 1000;
    private static final int EMPTY_HEALTH = 0;

    public Health() {
        super(FULL_HEALTH);
    }

    public void increase(Level level) {
        if(this.amount > 0){
            heal(level);
        }
    }

    public void heal(Level level) {
        this.amount += level.amount;
        if(this.amount >= FULL_HEALTH){
            this.amount = FULL_HEALTH;
        }
    }

    public void decrease(Level level) {
        if(this.amount > 0){
            hit(level);
        }
    }

    public void hit(Level level) {
        this.amount -= level.amount;
        if(this.amount <= EMPTY_HEALTH){
            this.amount = EMPTY_HEALTH;
            level.levelUp();
        }
    }
}
