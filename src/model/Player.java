package model;

public class Player {

    private final Health health;
    private final Level level;

    public Player() {
        this.health = new Health();
        this.level = new Level();
    }

    public void attack(Player target) {
        decreaseHealth(target.health, this.level);
    }

    public void decreaseHealth(Health targetHealth, Level level) {
        targetHealth.decrease(level);
    }

    public void heal(Player target) {
        increaseHealth(target.health, this.level);
    }

    public void increaseHealth(Health targetHealth, Level level) {
        targetHealth.increase(level);
    }

    @Override
    public String toString() {
        return "hp=" + health.amount +
                " | level=" + level.amount;
    }
}
