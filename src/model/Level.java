package model;

public class Level extends Jauge {

    private static final int LEVEL_INITIAL = 1;

    public Level() {
        super(LEVEL_INITIAL);
    }

    public void levelUp() {
        this.amount++;
    }
}
