package game;

import model.Player;

import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Player player1 = new Player();
        Player player2 = new Player();

        System.out.println("Player 1 : " + player1);
        System.out.println("Player 2 : " + player2);

        while(true) {

            System.out.println("Attaquer ou soigner ? (a/s) : ");
            String action = scanner.nextLine();

            if(action.equals("a")){
                player1.attack(player2);
            } else {
                player1.heal(player2);
            }

            System.out.println("Player 1 : " + player1);
            System.out.println("Player 2 : " + player2);
        }
    }
}
